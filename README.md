# EEE3088_Project

This is the repository eee3088F project. which is the PiHAT. The design will be for a motor feedback sensor. 

The microHAT will be able to drive a small motor and provide feedback on the position and speed of the motor. The microHAT will consist of a voltage regulator circuit, a power supply, a motor that provides position and speed feedback, LEDs which indicate the different statuses of the system (on/off, if battery needs to be replaced and motor direction) and a ZVD circuit to protect the Raspberry Pi Zero. Some scenarios in which the microHAT would be useful are: remote controlled cars, a moving solar panel, shop counter conveyor belt, motorised curtains, projector screens, small garage doors, etc
The PiHat will be attached to Raspberry Pi

Bill of Materials

Voltage regulator circuit
    9 V battery x 1
    capacitors 
        1 x 1 microfarad
        1 x 0.01 microfarads
        1 x 10 microfarads
    IC LT1761-5 x 1

ZVD circuitary
    MOSFET DMG2301L x 1
    PNP transistors BC560 x 2
    RESISTORs x 2

MOTOR IC and Servo motor x 1

Amplifier circuitary
    3 x 1k ohm resistors
    1 x 330 ohm resistors 
    1 x power supply 1.65 V
    2 x op amps 
    2 x voltage sources +15 V and -15V

Status LED 
    3 x LED's with 5 V breakdown 
    1 x 500 ohm resistor
    1 x 1000 ohm resistor 
    1 x zener diode with voltage breakdown 5V

Contributions can be made by adding waysin which the ciruitary can be improved. simplier ways to execute the same functionality of the above stated circuitary

